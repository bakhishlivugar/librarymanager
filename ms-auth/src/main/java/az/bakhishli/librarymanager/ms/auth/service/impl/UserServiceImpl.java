package az.bakhishli.librarymanager.ms.auth.service.impl;

import az.bakhishli.common.security.constants.UserAuthority;
import az.bakhishli.librarymanager.ms.auth.domain.Authority;
import az.bakhishli.librarymanager.ms.auth.domain.User;
import az.bakhishli.librarymanager.ms.auth.repository.UserRepository;
import az.bakhishli.librarymanager.ms.auth.service.UserService;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.AddAuthorityRequestDto;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.SignUpDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static az.bakhishli.common.security.constants.UserAuthority.ADMIN;
import static az.bakhishli.common.security.constants.UserAuthority.USER;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void signUp(SignUpDto signUpDto) {
        User user = createUserEntityObject(signUpDto, createAuthorities(USER.name()));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void addAuthority(AddAuthorityRequestDto requestDto) {
        User user = userRepository.findById(requestDto.getUserId())
                .orElseThrow(() -> new RuntimeException("User not found!"));
        Set<Authority> userAuthorities = user.getAuthorities();
        Set<String> authority = requestDto.getAuthority();
        for (String authorityString : authority) {
            userAuthorities.add(new Authority(authorityString));
        }
        user.setAuthorities(userAuthorities);
        userRepository.save(user);

    }

    private Set<Authority> createAuthorities(String... authoritiesString) {
        Set<Authority> authorities = new HashSet<>();
        for (String authorityString : authoritiesString) {
            authorities.add(new Authority(authorityString));
        }
        return authorities;
    }


    private User createUserEntityObject(SignUpDto signUpDto, Set<Authority> authorities) {
        User user = new User();
        user.setUsername(signUpDto.getUsername());
        user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
        user.setEmail(signUpDto.getEmail());
        user.setAuthorities(authorities);
        userRepository.save(user);
        return user;
    }

}
