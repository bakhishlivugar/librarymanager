package az.bakhishli.librarymanager.ms.auth.service;

import az.bakhishli.librarymanager.ms.auth.domain.Authority;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.AddAuthorityRequestDto;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.SignUpDto;

public interface UserService {
    void signUp(SignUpDto signUpDto);
    void addAuthority(AddAuthorityRequestDto requestDto);
}
