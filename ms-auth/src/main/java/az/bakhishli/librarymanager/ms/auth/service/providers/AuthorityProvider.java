package az.bakhishli.librarymanager.ms.auth.service.providers;

import az.bakhishli.common.security.auth.service.ClaimSet;
import az.bakhishli.common.security.auth.service.ClaimSetProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AuthorityProvider implements ClaimSetProvider {
    private static final String RULE = "authority";

    @Override
    public ClaimSet provide(Authentication authentication) {
        log.trace("Providing claims");
        Set<String> authorities = authentication.getAuthorities().stream()
                .map(grantedAuthority -> grantedAuthority.toString())
                .collect(Collectors.toSet());
        return new ClaimSet(RULE, authorities);
    }


    //before
//    @Override
//    public ClaimSet provide(Authentication authentication) {
//        log.trace("Providing {} claims", RULE);
//        String authority = authentication.getAuthorities().stream()
//                .map(grantedAuthority -> grantedAuthority.toString())
//                .findFirst()
//                .orElse("");
//        return new ClaimSet(RULE, authority);
//    }

}
