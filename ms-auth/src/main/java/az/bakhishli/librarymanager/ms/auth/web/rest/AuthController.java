package az.bakhishli.librarymanager.ms.auth.web.rest;

import az.bakhishli.common.security.auth.service.JwtService;
import az.bakhishli.librarymanager.ms.auth.domain.Authority;
import az.bakhishli.librarymanager.ms.auth.service.UserService;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.AddAuthorityRequestDto;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.LoginRequestDto;
import az.bakhishli.librarymanager.ms.auth.service.dto.auth.SignUpDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Duration;

@Slf4j
@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private static final Duration ONE_DAY = Duration.ofDays(1);
    private static final Duration ONE_WEEK = Duration.ofDays(7);

    private final JwtService jwtService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserService userService;

    @PostMapping("/sign-in")
    public ResponseEntity<AccessTokenDto> authorize(@Valid @RequestBody LoginRequestDto loginRequest) {
        log.trace("Authenticating user {}", loginRequest.getUsername());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                loginRequest.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject()
                .authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtService.issueToken(authentication, ONE_DAY);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + jwt);
        return new ResponseEntity<>(new AccessTokenDto(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<Void> signUp(@RequestBody @Validated SignUpDto dto) {
        log.trace("Sign up request with email {}", dto.getEmail());
        userService.signUp(dto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/add-authority")
    public ResponseEntity<Void> addAuthority(@RequestBody AddAuthorityRequestDto requestDto){
        userService.addAuthority(requestDto);
        return ResponseEntity.ok().build();
    }

}
