package az.bakhishli.librarymanager.ms.auth;

import az.bakhishli.common.security.auth.service.JwtService;
import az.bakhishli.librarymanager.ms.auth.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class LibraryManagerMsAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(LibraryManagerMsAuthApplication.class, args);
    }

}
