package az.bakhishli.librarymanager.ms.auth.service.dto.auth;


import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static az.bakhishli.common.security.constants.AuthConstants.PASSWORD_MAX_LENGTH;
import static az.bakhishli.common.security.constants.AuthConstants.PASSWORD_MIN_LENGTH;

@Data
@ToString(exclude = "password")
public class LoginRequestDto {

    @NotBlank
    private String username;

    @NotBlank
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;
}
