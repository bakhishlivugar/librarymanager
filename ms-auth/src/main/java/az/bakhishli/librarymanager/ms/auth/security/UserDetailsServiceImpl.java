package az.bakhishli.librarymanager.ms.auth.security;

import az.bakhishli.common.security.auth.service.CustomSpringSecurityUser;
import az.bakhishli.librarymanager.ms.auth.domain.User;
import az.bakhishli.librarymanager.ms.auth.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.trace("Authenticating: {}", username);

        return userRepository.findByUsername(username)
                .map(user -> createSpringSecurityUser(user))
                .orElseThrow(() ->
                        new UsernameNotFoundException(
                                String.format("User %s was not found in the database", username)));
    }


    private CustomSpringSecurityUser createSpringSecurityUser(User user) {
        List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                .collect(Collectors.toList());

        return new CustomSpringSecurityUser(
                user.getEmail(),
                user.getPassword(),
                grantedAuthorities);
    }

}
