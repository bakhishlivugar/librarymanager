package az.bakhishli.librarymanager.ms.auth.security;


import az.bakhishli.common.security.auth.AuthEntryPointJwt;
import az.bakhishli.common.security.auth.service.JwtService;
import az.bakhishli.common.security.auth.service.TokenAuthService;
import az.bakhishli.common.security.config.BaseSecurityConfig;
import az.bakhishli.common.security.config.SecurityProperties;
import az.bakhishli.common.security.constants.UserAuthority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

import static az.bakhishli.common.security.constants.UserAuthority.ADMIN;
import static az.bakhishli.common.security.constants.UserAuthority.USER;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class, AuthEntryPointJwt.class})
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends BaseSecurityConfig {

    public SecurityConfiguration(SecurityProperties securityProperties, JwtService jwtService) {
        super(securityProperties, List.of(new TokenAuthService(jwtService)));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/auth/sign-in",
                        "/api/auth/sign-up")
                .permitAll()
                .antMatchers(POST, "/api/auth/add-authority").access(authorities(ADMIN))
                .antMatchers(GET, "/api/test/user").access(authorities(USER))
                .anyRequest().authenticated();
        super.configure(http);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
