package az.bakhishli.librarymanager.ms.auth.service.dto.auth;

import az.bakhishli.common.security.constants.UserAuthority;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddAuthorityRequestDto {
    private Long userId;
    private Set<String> authority;
}
