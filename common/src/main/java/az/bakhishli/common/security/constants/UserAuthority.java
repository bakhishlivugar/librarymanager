package az.bakhishli.common.security.constants;

public enum UserAuthority {
    ADMIN, USER, PUBLISHER;
}
