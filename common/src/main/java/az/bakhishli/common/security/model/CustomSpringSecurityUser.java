package az.bakhishli.common.security.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Getter
@Setter
public class CustomSpringSecurityUser extends User {
    private static final long serialVersionUID = 3522416053866116034L;

    public CustomSpringSecurityUser(String username, String password,
                                    Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}
