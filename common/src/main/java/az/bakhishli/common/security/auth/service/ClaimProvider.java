package az.bakhishli.common.security.auth.service;

import org.springframework.security.core.Authentication;

public interface ClaimProvider {
    Claim provide(Authentication authentication);
}
