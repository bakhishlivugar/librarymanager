package az.bakhishli.common.security.auth.service;

import org.springframework.security.core.Authentication;

public interface ClaimSetProvider {
    ClaimSet provide(Authentication authentication);
}
