package az.bakhishli.common.security.auth.service;

import az.bakhishli.common.security.constants.HttpConstants;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static az.bakhishli.common.security.constants.HttpConstants.BEARER_AUTH_HEADER;

@Slf4j
@Service
@RequiredArgsConstructor
public final class TokenAuthService implements AuthService{

    private static final String AUTHORITY_CLAIM = "authority";
    private final JwtService jwtService;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest httpServletRequest) {
        return Optional.ofNullable(httpServletRequest.getHeader(HttpConstants.AUTH_HEADER))
                .filter(header -> isBearerAuth(header))
                .flatMap(header1 -> getAuthenticationBearer(header1));
    }

    private boolean isBearerAuth(String header) {
        return header.toLowerCase().startsWith(BEARER_AUTH_HEADER.toLowerCase());
    }

    private Optional<Authentication> getAuthenticationBearer(String header) {
        var token = header.substring(BEARER_AUTH_HEADER.length()).trim();
        var claims = jwtService.parseToken(token);
        log.info("The claims parsed {}", claims);
        if (claims.getExpiration().before(new Date())) {
            return Optional.empty();
        }
        return Optional.of(getAuthenticationBearer(claims));
    }

//    private Authentication getAuthenticationBearer(Claims claims) {
//        var roles = List.of(claims.get(AUTHORITY_CLAIM, String.class));
//        var authorityList = roles
//                .stream()
//                .map(role -> new SimpleGrantedAuthority(role))
//                .collect(Collectors.toList());
//
//        var details = new CustomSpringSecurityUser(
//                claims.getSubject(),
//                "",
//                authorityList
//        );
//
//        return new UsernamePasswordAuthenticationToken(claims.getSubject(), details, authorityList);
//    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> roles = claims.get(AUTHORITY_CLAIM, List.class);
        List<GrantedAuthority> authorityList = roles
                .stream()
                .map(a -> new SimpleGrantedAuthority(a.toString()))
                .collect(Collectors.toList());

        var details = new CustomSpringSecurityUser(
                claims.getSubject(),
                "",
                authorityList
        );

        return new UsernamePasswordAuthenticationToken(claims.getSubject(), details, authorityList);
    }

    //TODO: Jwtservice errorun handle etmek

}
