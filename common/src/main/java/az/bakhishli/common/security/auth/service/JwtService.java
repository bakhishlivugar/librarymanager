package az.bakhishli.common.security.auth.service;

import az.bakhishli.common.security.config.SecurityProperties;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

@Component
@Slf4j
@RequiredArgsConstructor
public final class JwtService {

    private final Set<ClaimSetProvider> claimSetProviders;
    private final SecurityProperties applicationProperties;
    private Key key;


    @PostConstruct
    public void init() {
        byte[] keyBytes;
        keyBytes = Decoders.BASE64.decode(applicationProperties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


    public String issueToken(Authentication authentication, Duration duration) {
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject((authentication.getName()))
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(duration)))
                .setHeader(Map.of("type","JWT"))
                .signWith(key, SignatureAlgorithm.HS512);

                return addClaimsSets(jwtBuilder, authentication).compact();
    }

    private JwtBuilder addClaimsSets(JwtBuilder jwtBuilder, Authentication authentication) {
        claimSetProviders.forEach(claimSetProvider -> {
            final ClaimSet claimSet = claimSetProvider.provide(authentication);
            log.trace("Adding claim {}", claimSet);
            jwtBuilder.claim(claimSet.getKey(), claimSet.getClaims());
        });
        return jwtBuilder;
    }

    //before
//    private JwtBuilder addClaims(JwtBuilder jwtBuilder, Authentication authentication) {
//        claimProviders.forEach(claimProvider -> {
//            final ClaimSet claim = claimProvider.provide(authentication);
//            log.trace("Adding claim {}", claim);
//            jwtBuilder.claim(claim.getKey(), claim.getClaim());
//        });
//        return jwtBuilder;
//    }


}
