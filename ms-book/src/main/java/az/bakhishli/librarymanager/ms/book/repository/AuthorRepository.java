package az.bakhishli.librarymanager.ms.book.repository;

import az.bakhishli.librarymanager.ms.book.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
}
