package az.bakhishli.librarymanager.ms.book.web.rest;

import az.bakhishli.librarymanager.ms.book.domain.Book;
import az.bakhishli.librarymanager.ms.book.service.dto.req.AddPublisherToBooksRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.req.BookRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.req.BookUpdateDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.BookDetailsResponseDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.BookResponseDto;
import az.bakhishli.librarymanager.ms.book.service.impl.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookController {
    public final BookServiceImpl bookService;

    @GetMapping("/allBooks/{pageNo}/{pageSize}")
    public ResponseEntity<List<BookResponseDto>> getAllBooks(@PathVariable Integer pageNo, @PathVariable Integer pageSize){
        return ResponseEntity.ok(bookService.getAllBooks(pageNo, pageSize));
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<BookDetailsResponseDto> getById(@PathVariable Long id){
        return ResponseEntity.ok(bookService.getById(id));
    }

    @GetMapping("/search/{title}")
    public ResponseEntity<List<BookResponseDto>> searchByTitle(@PathVariable String title, Pageable pageable){
        return ResponseEntity.ok(bookService.searchByTitle(title, pageable));
    }

//    @PostMapping("/addPublisherToBooks")
//    public ResponseEntity<List<BookResponseDto>> addPublisherToBooks(@RequestBody AddPublisherToBooksRequestDto dto){
//        return ResponseEntity.status(CREATED).body(bookService.addPublisherToBooks(dto));
//    }

    @GetMapping("/publisherBooks/{id}")
    public ResponseEntity<List<BookDetailsResponseDto>> getAllBooksByPublisherId(@PathVariable Long id){
        return ResponseEntity.ok(bookService.getAllBooksByPublisherId(id));
    }

    @PostMapping("/addBook")
    public ResponseEntity<BookDetailsResponseDto> create(@RequestBody BookRequestDto dto){
        return ResponseEntity.status(CREATED).body(bookService.create(dto));
    }

    @PutMapping("/updateBook/{id}")
    public ResponseEntity<BookResponseDto> update(@PathVariable Long id, @RequestBody BookUpdateDto bookUpdateDto){
        return ResponseEntity.ok().body(bookService.update(id, bookUpdateDto));
    }

}
