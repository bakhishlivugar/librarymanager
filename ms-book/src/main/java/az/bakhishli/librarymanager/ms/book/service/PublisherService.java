package az.bakhishli.librarymanager.ms.book.service;

import az.bakhishli.librarymanager.ms.book.domain.Publisher;
import az.bakhishli.librarymanager.ms.book.service.dto.req.PublisherRequestDto;

public interface PublisherService {
    Publisher create(PublisherRequestDto requestDto);
    Publisher getById(Long id);
    void delete(Long id);
}
