package az.bakhishli.librarymanager.ms.book.config;

import az.bakhishli.common.security.auth.service.AuthService;
import az.bakhishli.common.security.config.BaseSecurityConfig;
import az.bakhishli.common.security.config.SecurityProperties;
import az.bakhishli.common.security.constants.UserAuthority;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.util.List;

import static az.bakhishli.common.security.constants.HttpConstants.SUB_PATH;
import static az.bakhishli.common.security.constants.UserAuthority.PUBLISHER;
import static az.bakhishli.common.security.constants.UserAuthority.USER;
import static org.springframework.http.HttpMethod.POST;

@Slf4j
@EnableWebSecurity
public class SecurityConfiguration extends BaseSecurityConfig {
    private static final String BOOK_API = "/api/books";

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(POST,BOOK_API + "/addBook").access(authorities(PUBLISHER))
                .antMatchers(BOOK_API + SUB_PATH).access(authorities(USER, PUBLISHER));

        super.configure(http);
    }
}
