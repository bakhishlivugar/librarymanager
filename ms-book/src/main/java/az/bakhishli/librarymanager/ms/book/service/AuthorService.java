package az.bakhishli.librarymanager.ms.book.service;

import az.bakhishli.librarymanager.ms.book.service.dto.req.AuthorRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.AuthorResponseDto;

public interface AuthorService {
    AuthorResponseDto getById(Long id);
    AuthorResponseDto create(AuthorRequestDto authorRequestDto);
    void delete(Long id);

}
