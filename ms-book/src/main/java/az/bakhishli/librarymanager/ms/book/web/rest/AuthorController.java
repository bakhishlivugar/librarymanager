package az.bakhishli.librarymanager.ms.book.web.rest;

import az.bakhishli.librarymanager.ms.book.service.dto.req.AuthorRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.AuthorResponseDto;
import az.bakhishli.librarymanager.ms.book.service.impl.AuthorServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorServiceImpl authorService;

    @GetMapping("/{id}")
    public ResponseEntity<AuthorResponseDto> getById(@PathVariable Long id){
        return ResponseEntity.ok(authorService.getById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<AuthorResponseDto> create(@RequestBody AuthorRequestDto requestDto){
        return ResponseEntity.status(CREATED).body(authorService.create(requestDto));
    }

    @DeleteMapping
    public void delete(Long id){
        authorService.delete(id);
    }

}
