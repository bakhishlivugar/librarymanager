package az.bakhishli.librarymanager.ms.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryManagerMsBook {
    public static void main(String[] args) {
        SpringApplication.run(LibraryManagerMsBook.class, args);
    }
}
