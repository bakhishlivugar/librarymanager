package az.bakhishli.librarymanager.ms.book.service.dto.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookRequestDto {

    @NotBlank
    private String title;
    private String description;
    private List<Long> authorsId;
    private Long publisherId;

}
