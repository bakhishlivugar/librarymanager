package az.bakhishli.librarymanager.ms.book.service.dto.res;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookDetailsResponseDto {
    private Long id;
    private String title;
    private String description;
    private List<Long> authorsId;
    private Long publisherId;
}
