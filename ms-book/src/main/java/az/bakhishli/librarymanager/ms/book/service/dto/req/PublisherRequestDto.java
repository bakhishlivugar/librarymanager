package az.bakhishli.librarymanager.ms.book.service.dto.req;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublisherRequestDto {
    private Long id;
    private String name;
    private String address;
    private Long currentUserId;
}
