package az.bakhishli.librarymanager.ms.book.repository;

import az.bakhishli.librarymanager.ms.book.domain.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
}
