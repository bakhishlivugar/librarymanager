package az.bakhishli.librarymanager.ms.book.service.dto.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddPublisherToBooksRequestDto {
    private List<Long> booksId;
    private Long publisherId;
}
