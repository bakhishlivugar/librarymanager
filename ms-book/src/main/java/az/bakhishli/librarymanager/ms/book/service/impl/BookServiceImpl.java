package az.bakhishli.librarymanager.ms.book.service.impl;

import az.bakhishli.librarymanager.ms.book.domain.Author;
import az.bakhishli.librarymanager.ms.book.domain.Book;
import az.bakhishli.librarymanager.ms.book.domain.Publisher;
import az.bakhishli.librarymanager.ms.book.repository.AuthorRepository;
import az.bakhishli.librarymanager.ms.book.repository.BookRepository;
import az.bakhishli.librarymanager.ms.book.repository.PublisherRepository;
import az.bakhishli.librarymanager.ms.book.service.BookService;
import az.bakhishli.librarymanager.ms.book.service.dto.req.AddPublisherToBooksRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.req.BookRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.req.BookUpdateDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.BookDetailsResponseDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.BookResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final PublisherRepository publisherRepository;


    @Override
    public List<BookResponseDto> getAllBooks(Integer pageNo, Integer pageSize) {
        Page<Book> books = bookRepository.findAll(PageRequest.of(pageNo, pageSize));
        List<BookResponseDto> responseDtos = new ArrayList<>();
        List<Book> collect = books.get().collect(Collectors.toList());

        for (int i = 0; i < books.getSize(); i++){
            BookResponseDto bookResponseDto = new BookResponseDto();
            Book book = collect.get(i);
            bookResponseDto.setId(book.getId());
            bookResponseDto.setTitle(book.getTitle());
            bookResponseDto.setDescription(book.getDescription());
            if(book.getPublisher() != null){
                bookResponseDto.setPublisherId(book.getPublisher().getId());
            }
            responseDtos.add(bookResponseDto);
        }
        return responseDtos;
    }

    @Override
    public BookDetailsResponseDto create(BookRequestDto bookRequestDto) {
        Book book = new Book();

        book.setTitle(bookRequestDto.getTitle());
        book.setDescription(bookRequestDto.getDescription());

        Publisher publisher = publisherRepository.findById(bookRequestDto.getPublisherId())
                .orElseThrow(() -> new RuntimeException("Publisher with given id not found"));

        book.setPublisher(publisher);

        // Add authors to Book
        List<Author> authors = new ArrayList<>();
        List<Long> authorsId = new ArrayList<>(bookRequestDto.getAuthorsId());

        for(int i = 0; i < authorsId.size(); i++){
            authors.add(new Author());
            authors.get(i).setId(authorsId.get(i));
            book.setAuthors(authors);
        }


        // Save to db
        Book save = bookRepository.save(book);

        BookDetailsResponseDto responseDto = new BookDetailsResponseDto();
        responseDto.setId(save.getId());
        responseDto.setTitle(save.getTitle());
        responseDto.setDescription(save.getDescription());
        responseDto.setPublisherId(save.getPublisher().getId());
        // Set authors id to responseDto
        int size = save.getAuthors().size();
        for(int i = 0; i < size; i++){
            responseDto.setAuthorsId(authorsId);
        }
        return responseDto;
    }

    @Transactional
    @Override
    public BookDetailsResponseDto getById(Long id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Book not found with given id"));

        BookDetailsResponseDto responseDto = new BookDetailsResponseDto();
        responseDto.setId(book.getId());
        responseDto.setTitle(book.getTitle());
        responseDto.setDescription(book.getDescription());

        List<Long> authorsId = book.getAuthors().stream().map(Author::getId).collect(Collectors.toList());
        responseDto.setAuthorsId(authorsId);


        if (book.getPublisher() != null){
            responseDto.setPublisherId(book.getPublisher().getId());
        }

        return responseDto;
    }

    @Override
    public List<BookResponseDto> searchByTitle(String title, Pageable pageable) {
        List<Book> books = bookRepository.searchByTitleLike(title, pageable);
        List<BookResponseDto> responseDtoList = new ArrayList<>();

        for(int i = 0; i < books.size(); i++){
            Book book = books.get(i);
            BookResponseDto bookResponseDto = new BookResponseDto();
            bookResponseDto.setId(book.getId());
            bookResponseDto.setTitle(book.getTitle());
            bookResponseDto.setDescription(book.getDescription());
            if(book.getPublisher() != null) {
                bookResponseDto.setPublisherId(book.getPublisher().getId());
            }
            responseDtoList.add(bookResponseDto);
        }
        return responseDtoList;

    }

    @Transactional
    @Override
    public List<BookDetailsResponseDto> getAllBooksByPublisherId(Long id) {
        Publisher publisher = publisherRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Publisher was not found with given id"));

        List<Book> books = publisher.getBooks();

        List<BookDetailsResponseDto> bookResponseDto = new ArrayList<>();

        for(int i = 0; i < books.size(); i++){
            List<Long> authorsId = books.get(i).getAuthors().stream().map(Author::getId).collect(Collectors.toList());
            Book book = books.get(i);
            BookDetailsResponseDto responseDto = new BookDetailsResponseDto();
            responseDto.setId(book.getId());
            responseDto.setTitle(book.getTitle());
            responseDto.setDescription(book.getDescription());
            responseDto.setPublisherId(book.getPublisher().getId());
            responseDto.setAuthorsId(authorsId);
            bookResponseDto.add(responseDto);
        }

        return bookResponseDto;
    }

    @Override
    public BookResponseDto update(Long id, BookUpdateDto bookUpdateDto) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Book not found!"));



        book.setTitle(bookUpdateDto.getTitle());
        book.setDescription(bookUpdateDto.getDescription());

        Book save = bookRepository.save(book);

        BookResponseDto responseDto = new BookResponseDto();
        responseDto.setTitle(save.getTitle());
        responseDto.setDescription(save.getDescription());
        responseDto.setPublisherId(save.getPublisher().getId());

        return responseDto;
    }

//    @Transactional
//    @Override
//    public List<BookResponseDto> addPublisherToBooks(AddPublisherToBooksRequestDto dto) {
//
//        List<Book> books = bookRepository.findAllById(dto.getBooksId());
//        List<BookResponseDto> responseDto = new ArrayList<>();
//
//        Publisher publisher = new Publisher();
//        publisher.setId(dto.getPublisherId());
//
//        for (int i = 0; i < books.size(); i++) {
//            Book book = books.get(i);
//            book.setPublisher(publisher);
//
//            //Convert to Dto List
//            BookResponseDto bookResponseDto = new BookResponseDto();
//            bookResponseDto.setId(book.getId());
//            bookResponseDto.setTitle(book.getTitle());
//            bookResponseDto.setDescription(book.getDescription());
//            bookResponseDto.setPublisherId(book.getPublisher().getId());
//            responseDto.add(bookResponseDto);
//        }
//        return responseDto;
//
//    }


}
