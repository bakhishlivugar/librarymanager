package az.bakhishli.librarymanager.ms.book.service;

import az.bakhishli.librarymanager.ms.book.domain.Book;
import az.bakhishli.librarymanager.ms.book.service.dto.req.AddPublisherToBooksRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.req.BookRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.req.BookUpdateDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.BookDetailsResponseDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.BookResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BookService {
    List<BookResponseDto> getAllBooks(Integer pageNo, Integer pageSize);
    BookDetailsResponseDto create(BookRequestDto bookRequestDto);
    BookDetailsResponseDto getById(Long id);
    List<BookResponseDto> searchByTitle(String title, Pageable pageable);
    //List<BookResponseDto> addPublisherToBooks(AddPublisherToBooksRequestDto dto);
    List<BookDetailsResponseDto> getAllBooksByPublisherId(Long id);
    BookResponseDto update(Long id, BookUpdateDto bookUpdateDto);
}
