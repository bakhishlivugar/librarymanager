package az.bakhishli.librarymanager.ms.book.service.impl;

import az.bakhishli.librarymanager.ms.book.domain.Publisher;
import az.bakhishli.librarymanager.ms.book.repository.PublisherRepository;
import az.bakhishli.librarymanager.ms.book.service.PublisherService;
import az.bakhishli.librarymanager.ms.book.service.dto.req.PublisherRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PublisherServiceImpl implements PublisherService {
    private final PublisherRepository publisherRepository;

    @Override
    public Publisher create(PublisherRequestDto requestDto) {

        Publisher publisher = new Publisher();
        publisher.setId(requestDto.getId());
        publisher.setName(requestDto.getName());
        publisher.setAddress(requestDto.getAddress());

        //publisher.setUserId();

        return publisherRepository.save(publisher);
    }

    @Override
    public Publisher getById(Long id) {
        return publisherRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Publisher was not found with given id"));
    }

    @Override
    public void delete(Long id) {
        publisherRepository.deleteById(id);
    }
}
