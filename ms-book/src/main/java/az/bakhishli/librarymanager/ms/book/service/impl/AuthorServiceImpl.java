package az.bakhishli.librarymanager.ms.book.service.impl;

import az.bakhishli.librarymanager.ms.book.domain.Author;
import az.bakhishli.librarymanager.ms.book.repository.AuthorRepository;
import az.bakhishli.librarymanager.ms.book.service.AuthorService;
import az.bakhishli.librarymanager.ms.book.service.dto.req.AuthorRequestDto;
import az.bakhishli.librarymanager.ms.book.service.dto.res.AuthorResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;

    @Override
    public AuthorResponseDto getById(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new RuntimeException("Author not found with given id"));
        AuthorResponseDto responseDto = new AuthorResponseDto();
        responseDto.setId(author.getId());
        responseDto.setFullName(author.getFullName());
        return responseDto;
    }

    @Override
    public AuthorResponseDto create(AuthorRequestDto authorRequestDto) {
        Author author = new Author();
        author.setFullName(authorRequestDto.getFullName());
        authorRepository.save(author);

        AuthorResponseDto responseDto = new AuthorResponseDto();
        responseDto.setId(author.getId());
        responseDto.setFullName(author.getFullName());

        return responseDto;
    }

    @Override
    public void delete(Long id) {
        authorRepository.deleteById(id);
    }
}
