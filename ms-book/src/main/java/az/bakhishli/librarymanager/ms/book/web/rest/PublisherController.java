package az.bakhishli.librarymanager.ms.book.web.rest;


import az.bakhishli.librarymanager.ms.book.domain.Publisher;
import az.bakhishli.librarymanager.ms.book.service.dto.req.PublisherRequestDto;
import az.bakhishli.librarymanager.ms.book.service.impl.PublisherServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/publishers")
@RequiredArgsConstructor
public class PublisherController {
    private final PublisherServiceImpl publisherService;

    @GetMapping("/{id}")
    public ResponseEntity<Publisher> getById(@PathVariable Long id){
        return ResponseEntity.ok(publisherService.getById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<Publisher> create(@RequestBody PublisherRequestDto dto){
        return ResponseEntity.status(CREATED).body(publisherService.create(dto));
    }

    @DeleteMapping
    public void delete(Long id){
        publisherService.delete(id);
    }
}
